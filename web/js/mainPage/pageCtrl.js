var pageCtrl = (function($){

    var view = pageViewGenerator($),
        countOfRoutes = -1;


    jQuery.extend(jQuery.validator.messages, {
        required: "Поле не может быть пустым",
        remote: "Please fix this field.",
        email: "Email не существует или не является валидным",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });

    function setShowPlacesToPage(data){
        view.generatePointsList($('.showPlacesList'), data);
        ++countOfRoutes;
    }

    function menuNavBlocksEvent(){
        var $menuItems = $('.tp-navMenu li a'),
            menuStruct = {},
            $parentBlock = $('.content-header'),
            $wrapArrow = $('.menu-arrow');

        $menuItems.each(function() {
            var $item = $(this);
            menuStruct[$item.data('name')] = {
                item: $item,
                dataBlock: $('.' + $item.data('block-class'))
            };
        });

        $menuItems.on('click', function(e){
            var itemName = $(this).data('name');

            for(var key in menuStruct){
                if(key === itemName){
                    menuStruct[key].dataBlock.show();
                    menuStruct[key].item.addClass('active');
                }else{
                    menuStruct[key].dataBlock.hide();
                    menuStruct[key].item.removeClass('active');
                }
            }

            if($parentBlock.hasClass('hide')){
                $wrapArrow.click();
            }

            e.preventDefault();
            return false;
        });
    }

    function wrapContentEvent(){
        var wrapArrow = $('.menu-arrow'),
            contentHeader = $('header.content-header');

        wrapArrow.on('click', function(){
            wrapArrow.toggleClass('menu-arrow-show menu-arrow-hide');

            if(contentHeader.hasClass('hide')){
                contentHeader.stop().animate({height: '100%'}, {duration: 1000});
                setTimeout(function(){
                    contentHeader.css('overflow', 'visible')
                }, 1000);
            }else{
                contentHeader.css('overflow', 'hidden');
                contentHeader.stop().animate({height: '110px'}, {duration: 1000});
            }

            contentHeader.toggleClass('hide show');
        });
    }

    function showPointItem(){
        var $showPlacesList = $('.showPlacesList'),
            currentPointIndex = -1;

        $showPlacesList.on('click', 'li', function(){
            var pointItemId = this.id.replace('showPlace-', ''),
                prevPoint,
                index;

            index = countOfRoutes > 0 ? currentPointIndex+1 : currentPointIndex;

            if(currentPointIndex !== -1 && mapCtrl.offShowPlaceInfoWindow(index)){
                prevPoint = document.getElementById('showPlace-' + currentPointIndex);
                prevPoint.className = prevPoint.className.replace('switchOn', '');
            }

            currentPointIndex = parseInt(pointItemId);
            index = countOfRoutes > 0 ? currentPointIndex+1 : currentPointIndex;
            mapCtrl.onShowPlaceInfoWindow(index);
            this.className += ' switchOn';
        });

    }

    function submitResponseFormEvent(){
        var $form = $('.response-form');

        /*$form.find('.response-btn').on('click', function(e){
            requestsCtrl.submitResponseForm($form);
            e.preventDefault();
        });*/

        $form.validate({
            rules:{
                responseName: {
                    required: true
                },
                responseEmail: {
                    required: true,
                    email: true
                },

                responseMessage: {
                    required: true
                }
            },
            errorClass: 'response-error',
            errorElement: 'div',
            submitHandler: function(nativeForm){
                var formElements = nativeForm.elements;

                requestsCtrl.submitResponseForm($form);

                for(var i = 0; i < formElements.length; ++i){
                    formElements[i].value = '';
                }

                $(".success-window").show();

                setTimeout(function(){
                    $(".success-window").hide();
                }, 2000);

                return false;
            }
        });
    }

    function initPageEvents(){
        menuNavBlocksEvent();
        wrapContentEvent();
        showPointItem();
        submitResponseFormEvent();
    }

    return {
        setShowPlacesToPage: setShowPlacesToPage,
        initPageEvents: initPageEvents
    };
})(jQuery);