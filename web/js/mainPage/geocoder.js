function geocoderObj(){
    var geocoder = new google.maps.Geocoder();

    function codeAddress(addressStr, callback){
        var point = {};

        geocoder.geocode({
            address: addressStr
        }, function(results, status){
            /*if(status === 'OK'){
                point['lat'] = results[0].geometry.location.lat();
                point['lon'] = results[0].geometry.location.lng();
            }

            point = null;
            alert("Geocode was not successful for the following reason: " + status);*/

            callback(results[0].geometry.location.lat(), results[0].geometry.location.lng());
        });

        return point;
    }

    function reverseCodeAddress(lat, lon, callback){
        var address = '';

        geocoder.geocode({
            'latLng': new google.maps.LatLng(lat, lon)
        }, function(results, status){
            /*if(status === 'OK'){*/
            callback(results[0].formatted_address);
            //alert("Geocoder failed due to: " + status);
        });
    }

    return {
        codeAddress: codeAddress,
        reverseCodeAddress: reverseCodeAddress
    };
}