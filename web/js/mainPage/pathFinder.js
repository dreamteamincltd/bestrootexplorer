(function($) {

    var objForPathFinder = {
        formParts: {
            navForm: $('.tp-navForm'),
            beginEndPoints: $('.tp-inputPlace'),
            fromInput: $('.tp-fromPlace'),
            fromCoordsInput: $('.tp-fromCoords'),
            toInput: $('.tp-toPlace'),
            toCoordsInput: $('.tp-toCoords'),
            placeType: $('.tp-typeSP'),
            findButton: $('.tp-findButton')
        },
        map: mapCtrl,
        requests: requestsCtrl,
        page: pageCtrl
    };

    (function pathFinder(options) {
        var $navForm = options.formParts.navForm,
            $beginEndPoints = options.formParts.beginEndPoints,
            $placeType = options.formParts.placeType,
            $findButton = options.formParts.findButton,
            $fromInput = options.formParts.fromInput,
            $fromCoordsInput = options.formParts.fromCoordsInput,
            $toInput = options.formParts.toInput,
            $toCoordsInput = options.formParts.toCoordsInput,
            requests = options.requests,
            map = options.map,
            page = options.page,
            $inputCont = $beginEndPoints.parent();

        function show(data){
            map.findPath(data);
            page.setShowPlacesToPage(data);
        }

        function inputFieldsKeyUp($input){
            var pointType = '';

            if($input.hasClass('tp-fromPlace')){
                pointType = 'from';
            }else if($input.hasClass('tp-toPlace')){
                pointType = 'to';
            }

            if(!$input.val()) {
                map.defineMarker($input.val(), pointType);
            }

            /*if($fromInput.val() !== '' && $toInput.val() !== '') {
                requests.getNearShowPlaces($navForm, show);
            }*/
        }

        function checkInputFields(){
            if($fromInput.val()){
                map.defineMarker($fromInput.val(), 'from');
            }

            if($toInput.val()){
                map.defineMarker($toInput.val(), 'to');
            }

            if($fromCoordsInput.val() && $fromInput.val() && $toCoordsInput.val() && $toInput.val()){
                return true;
            }

            return false;
        }

        $beginEndPoints.data('timeout', null).keyup(function () {
            var $input = $(this);
            clearTimeout($input.data('timeout'));
            $input.data('timeout', setTimeout(function(){
                inputFieldsKeyUp($input);
            }, 800));
        });

        $beginEndPoints.data('timeout', null).on('fieldChanged', function () {
            var $input = $(this);
            clearTimeout($input.data('timeout'));
            $input.data('timeout', setTimeout(function(){
                if($fromInput.val() !== '' && $toInput.val() !== '') {
                    requests.getNearShowPlaces($navForm, show);
                }
            }, 1000));
        });


        /*$beginEndPoints.on('change', function(e){
            alert(1);
        });*/

        $placeType.data('timeout', null).click(function () {
            clearTimeout($(this).data('timeout'));
            $(this).data('timeout', setTimeout(function () {
                requests.getNearShowPlaces($navForm, show);
            }, 800))
        });

        $inputCont.find('.clear-btn').click(function(){
            var inputCont = $(this).parent();

            var inp = inputCont.find('.tp-inputPlace');

            inputCont.find('input').val('');
            inputFieldsKeyUp(inp);
        });

        $inputCont.find('.nav-btn').click(function(){

            if($(this).parent().hasClass('from-fields')){
                map.defineCurrentPosition('from');
            }else{
                map.defineCurrentPosition('to');
            }
        });

        map.fillingByClick({
            fromInput: $fromInput,
            fromCoordsInput: $fromCoordsInput,
            toInput: $toInput,
            toCoordsInput: $toCoordsInput
        });

        $findButton.on('click', submitNavForm);

        function submitNavForm(event) {
            if(checkInputFields()) {
                requests.getNearShowPlaces($navForm, show);
            }

            event.preventDefault();
        }
    })(objForPathFinder);

})(jQuery);