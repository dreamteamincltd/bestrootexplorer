(function($) {
    /**
     * Виджет сворачивания/разворачивания элементов
     *
     * @param options - объект вида:
     * {
     *  elem: jquery-коллекция элементов, на которых применяется эффект,
     *  size: размер элемента при развёртывании,
     *  wrapButton: jquery-коллекция элементов-кнопок,
     *  duration: длительность анимации (в мс)
     * }
     */
    function wrapper(options) {
        var elem = options.elem,
            wrapButton = options.wrapButton,
            duration = options.duration || 1000,
            size = options.size || 0;

        var sizeInt = parseInt(size, 10),
            leftShift = (100 - sizeInt);

        function wrap() {
            elem.stop().animate({
                left: '100%',
                width: 0
            }, duration);
        }

        function unwrap() {
            elem.stop().animate({
                left: leftShift + '%',
                width: size
            }, duration);
        }

        function buttonOnClick() {
            var button = $(this);

            button.toggleClass('selMenu-wrap selMenu-unwrap');

            if (button.hasClass('selMenu-unwrap')) {
                unwrap();
                return;
            }

            if (button.hasClass('selMenu-wrap')) {
                wrap();
            }
        }

        elem.on('wrap', wrap);
        elem.on('unwrap', unwrap);
        wrapButton.on('click', buttonOnClick);
    }

    wrapper({
        elem: $('.tp-rightAside'),
        size: '25%',
        wrapButton: $('.wrapButton'),
        duration: 1000
    });
})(jQuery);
