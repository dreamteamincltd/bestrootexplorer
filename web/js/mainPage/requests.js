var requestsCtrl = (function() {
    function getShowPlaces() {
        $.ajax({
            type: "POST",
            url: "/ajax",
            dataType: "json",
            success: function (data){
                console.log(data);
                mapCtrl.setShowPlaces(data);
                pageCtrl.setShowPlacesToPage(data);
            },
            error: function (jqXHR, errNo, errDes){
                console.log(errNo + " " + errDes);
            }
        });
    }

    function getNearShowPlaces($form, sucFunc) {
		console.log($form.serialize());
        $.ajax({
            type: "POST",
            url: "/ajax/way",
            dataType: "json",
            data: $form.serialize(),
            success: function(data) {
                /*console.log(data);*/
                sucFunc(data);
            },
            error: function (jqXHR, errNo, errDes) {
                console.log(errNo + " " + errDes);
            }
        });
    }

    function submitResponseForm($form){
        //console.log($form.serialize());
        $.ajax({
            type: "POST",
            url: "/ajax/mail",
            data: $form.serialize(),
            success: function() {
                console.log('response success');
            },
            error: function (jqXHR, errNo, errDes) {
                console.log(errNo + " " + errDes);
            }
        });
    }

    return {
        getShowPlaces: getShowPlaces,
        getNearShowPlaces: getNearShowPlaces,
        submitResponseForm: submitResponseForm
    };
})();