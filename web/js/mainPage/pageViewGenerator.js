function pageViewGenerator($){
    function generatePointsListItem(point, index){
        var lat = point.point.lat || 0,
            lon = point.point.lon || 0,
            describe = point.describe.text,
            imgSrc = point.describe.img,
            type = point.describe.type,
            imgElem = '';

        if(imgSrc === null){
            switch(type){
                case 'monument':
                    imgSrc = 'img/checkboxIcons/pamyatnikActive.png';
                    break;
                case 'museum':
                    imgSrc = 'img/checkboxIcons/museumActive.png';
                    break;
                case 'park':
                    imgSrc = 'img/checkboxIcons/parksActive.png';
                    break;
                case 'church':
                    imgSrc = 'img/checkboxIcons/hramActive.png';
                    break;
                default:
                    imgSrc = 'img/checkboxIcons/pamyatnikActive.png';
                    break;
            }
        }

        imgElem = '<img src="' + imgSrc + '">';

        return '<li class="point-item" id="showPlace-'+ index +'"><figure class="point-img">' + imgElem +
            '</figure><div class="point-descr">' + describe + '</div><div class="point-clearfix"></div></li>';
    }

    function generatePointsList($pointsListCont, points){
        var pointsList = '';

        for(var i = 0; i < points.length; ++i){
            pointsList += generatePointsListItem(points[i], i);
        }

        $pointsListCont.empty();
        $pointsListCont.append(pointsList);

        return pointsList;
    }

    return {
        generatePointsList: generatePointsList
    };
};