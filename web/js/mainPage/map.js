//TODO refactor code

var mapCtrl = (function(){
    var map,
        geocoder,
        directionsService = new google.maps.DirectionsService(),
        directionsDisplay = [],
        showPlaces = [],
        //точки конца и начала маршрута вместе с привязанными к ним полями
        points = {
            from: {
                marker: null,
                input: null,
                coordsInput: null
            },
            to: {
                marker: null,
                input: null,
                coordsInput: null
            }
        };

    function initialize() {
        map = new google.maps.Map(document.getElementById('tp-map'), {
            center: new google.maps.LatLng(59.9342802,30.3350986),
            zoom: 11,
            panControl: false,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            scaleControlOptions:{
                style: google.maps.ScaleControlStyle.SMALL,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            mapTypeControl: false,
            draggableCursor: 'default',
            draggingCursor: 'move',
            disableDoubleClickZoom: true
        });

        /*geocoder = new google.maps.Geocoder();*/
    }

    function createShowPlace(showPlaceObj) {
   //     console.log("point: " + showPlaceObj);
        console.log(showPlaceObj.describe);
        var lat = showPlaceObj.point.lat || 0,
            lon = showPlaceObj.point.lon || 0,
            name = showPlaceObj.describe.text,
            text = showPlaceObj.describe.description,
            imgSrc = showPlaceObj.describe.img,
            imgElem = '';

        if(imgSrc !== null){
            imgElem = '<img class="point_map_image" src = "' + imgSrc + '"></img>';
        }

        var showPlace = {
            marker: new google.maps.Marker({
                position: new google.maps.LatLng(lat, lon),
                map: map,
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    fillColor: "gray",
                    fillOpacity: 1,
                    scale: 5,
                    strokeColor: 'black',
                    strokeWeight: 1
                }
            }),

            infoWindow:  new google.maps.InfoWindow({
                content: '<div class="point_map">' + imgElem + '<h2>' + name + '</h2><p>' + text + '</p>' + '</div>'
                /*maxWidth: 300*/
            })
        };

        google.maps.event.addListener(showPlace.marker, 'mouseover', function(){
            showPlace.infoWindow.open(map, showPlace.marker);
        });

        google.maps.event.addListener(showPlace.marker, 'mouseout', function(){
            showPlace.infoWindow.close();
        });

        return showPlace;
    }

    function addShowPlaces(showPlacesList) {
        for(var i = 0; i < showPlacesList.length; ++i){
            showPlaces.push(createShowPlace(showPlacesList[i]));
        }
    }

    function clearShowPlaces() {
        for(var i = 0; i < showPlaces.length; ++i){
            google.maps.event.clearInstanceListeners(showPlaces[i].marker);
            showPlaces[i].marker.setMap(null);
            showPlaces[i].marker = null;
            showPlaces[i].infoWindow.setMap(null);
            showPlaces[i].infoWindow = null;
        }

        showPlaces = [];
    }

    function onShowPlaceInfoWindow(index){
        var showPlace = showPlaces[index];

        if(showPlace === undefined){
            return false;
        }

        showPlace.infoWindow.open(map, showPlace.marker);
        map.setCenter(showPlace.marker.getPosition());

        return true;
    }

    function offShowPlaceInfoWindow(index){
        var showPlace = showPlaces[index];

        if(showPlace === undefined){
            return false;
        }

        showPlace.infoWindow.close();
        return true;

    }

    function createBeginEndPoint(pointObj, type) {
        var lat = pointObj.point.lat || 0,
            lon = pointObj.point.lon || 0,
            text = (type === 'from') ? 'Начало пути' : 'Конец пути';

        var pointPlace = {
            marker: new google.maps.Marker({
                position: new google.maps.LatLng(lat, lon),
                map: map,
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    fillColor: "black",
                    fillOpacity: 1,
                    scale: 5,
                    strokeColor: 'gray',
                    strokeWeight: 1
                }
            }),

            infoWindow:  new google.maps.InfoWindow({
                content: '<div style="overflow: hidden !important; width: 100px"><p>' + text + '</p></div>'
                /*maxWidth: 700*/
            })
        };

        google.maps.event.addListener(pointPlace.marker, 'mouseover', function(){
            pointPlace.infoWindow.open(map, pointPlace.marker);
        });

        google.maps.event.addListener(pointPlace.marker, 'mouseout', function(){
            pointPlace.infoWindow.close();
        });

        showPlaces.push(pointPlace);
    }

    function buildRoute(start, end, routeDisplay) {
        var startPoint = showPlaces[start],
            endPoint = showPlaces[end],
            startLatLng = startPoint.marker.getPosition(),
            endLatLng = endPoint.marker.getPosition(),
            waypts = [],
            curPoint;

        for(var i = start + 1; i < end; ++i){
            curPoint = showPlaces[i];

            waypts.push({
                location: curPoint.marker.getPosition(),
                stopover: true
            });
        }

        var routeRequest = {
            origin: startLatLng,
            destination: endLatLng,
            travelMode: google.maps.TravelMode.WALKING,
            unitSystem: google.maps.UnitSystem.METRICAL,
            waypoints: waypts,
            optimizeWaypoints: true,
            region: 'ru'
        };

        directionsService.route(routeRequest, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                routeDisplay.setDirections(response);
            }else{
                console.log(status);
            }
        });
    }

    function addRouteDisplay(){
        directionsDisplay.push(new google.maps.DirectionsRenderer({
            map: map,
            suppressMarkers: true
        }));
    }

    function clearRouteDisplays(){
        for(var i = 0; i < directionsDisplay.length; ++i){
            directionsDisplay[i].setMap(null);
            directionsDisplay[i] = null;
        }

        directionsDisplay = [];
    }

    function calcRoute(start, end){
        var placesCount = ((end+1)-start),
            begin = start,
            routes = -1;

        clearRouteDisplays();

        if(placesCount <= 10){
            addRouteDisplay();
            ++routes;
            buildRoute(start, end,  directionsDisplay[routes]);
            return;
        }

        for(var i = begin; i < end; i = i+9){
            if(i > end) {
                i = end;
            }

            addRouteDisplay();
            ++routes;

            buildRoute(begin, i, directionsDisplay[routes]);
            begin = i;
        }

        if(i > end && begin < end) {
            addRouteDisplay();
            ++routes;
            buildRoute(begin, end, directionsDisplay[routes]);
        }
    }

    function setShowPlaces(showPlacesList) {
        clearShowPlaces();
        clearRouteDisplays();

        console.log("type " + (typeof showPlacesList));

        addShowPlaces(showPlacesList);
    }

    function findPath(pointsList) {
        if(pointsList.length === 0) {
            return;
        }

        clearShowPlaces();

        var beginPoint = pointsList.shift(),
            endPoint = pointsList.pop();

        if(endPoint.describe.text !== 'to'
            || endPoint.point.lat === undefined
            || endPoint.point.lon === undefined)
        {
            createBeginEndPoint(beginPoint, beginPoint.describe.text);
            setShowPlaces(pointsList);
        }else{
            createBeginEndPoint(beginPoint, beginPoint.describe.text);
            addShowPlaces(pointsList);
            createBeginEndPoint(endPoint, endPoint.describe.text);

            calcRoute(0, showPlaces.length-1);
        }
    }

    //создание маркера начальной или конечной точки
    function setMarker(point, lat, lng){
	//	var image = '/img/point.png';
        point.marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: map,
            draggable: true,
		//	icon: image,
	
        });

        //point.input.val(lat + ', ' + lng);

        geocoder.reverseCodeAddress(lat, lng, function(addressStr){
            point.input.val(addressStr);
            point.coordsInput.val(lat + ', ' + lng);
        });

        point.input.trigger('fieldChanged');

        google.maps.event.addListener(point.marker, 'click', function(){
            removePointMarker(point);
        });

        google.maps.event.addListener(point.marker, 'dragend', function(e){
            moveMarker(point, e.latLng.lat(), e.latLng.lng());
        });
    }

    function removePointMarker(point){
        point.marker.setMap(null);
        google.maps.event.clearInstanceListeners(point.marker);
        point.marker = null;

        point.input.val('');
        point.coordsInput.val('');
    }

    //Перемещение начальной или конечно точки
    function moveMarker(point, lat, lng) {
        point.marker.setPosition(new google.maps.LatLng(lat, lng));

        geocoder.reverseCodeAddress(lat, lng, function(addressStr){
            point.input.val(addressStr);
            point.coordsInput.val(lat + ', ' + lng);
        });

        point.input.trigger('fieldChanged');
    }

    function defineMarker(addressStr, pointType){
        if(addressStr == ''){
            if(points[pointType].marker !== null) removePointMarker(points[pointType]);
        }else{
            geocoder.codeAddress(addressStr, function (lat, lng) {
                defineMarkerFromField(new google.maps.LatLng(lat, lng), pointType);
            });
        }
    }

    //Определение действий с маркерами начальной и конечной точек
    function defineMarkerFromEvent(event) {
        var latLng = event.latLng;
         /*   isMouseEvent = (typeof coords === "object") && (coords.latLng !== null) && (coords.stop !== null);

        if(isMouseEvent) {
            latLng = coords.latLng;
        }else if(coords instanceof google.maps.LatLng){
            latLng = coords;
        }*/

        if (points.from.marker === null) {
            setMarker(points.from, latLng.lat(), latLng.lng());
            return;
        }

        if (points.to.marker === null) {
            setMarker(points.to, latLng.lat(), latLng.lng());
            return;
        }

        moveMarker(points.to, latLng.lat(), latLng.lng());
    }

    function defineMarkerFromField(coords, pointType){
        var point = points[pointType];

        if (point.marker === null) {
            setMarker(point, coords.lat(), coords.lng());
            return;
        }

        moveMarker(point, coords.lat(), coords.lng());
    }

    /**
     *
     * @param options - объект вида
     * {
     *  fromInput: jquery-колекция input
     *  toInput: jquery-колекция input
     * }
     */
    function fillingByClick(options) {
        var $fromInput = options.fromInput,
            $toInput = options.toInput,
            $toCoordsInput = options.toCoordsInput,
            $fromCoordsInput = options.fromCoordsInput;

        points.from.input = $fromInput;
        points.from.coordsInput = $fromCoordsInput;
        points.to.input = $toInput;
        points.to.coordsInput = $toCoordsInput;

        google.maps.event.addDomListener(window, 'load', function() {
            google.maps.event.addListener(map, 'click', defineMarkerFromEvent);
        });
    }

    function defineCurrentPosition(type){
        if(navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                points.from.coordsInput.val(position.coords.latitude + ', ' + position.coords.longitude);

                var initLoc = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
                defineMarkerFromField(initLoc, type);

                var locWindow = new google.maps.InfoWindow({
                    content: '<div style="width: 200px;">Вы здесь</div>'
                });

                /*points.from.marker.setDraggable(false);*/

                locWindow.open(map, points[type].marker);

                google.maps.event.addListener(points[type].marker, 'dragend', function(e){
                    if(locWindow) {
                        locWindow.close();
                        locWindow = null;
                    }
                });

                map.setCenter(initLoc);
            });
        }
    }

    function setGeocoder(gc){
        geocoder = gc;
    }

    return {
        init: initialize,
        setShowPlaces: setShowPlaces,
        onShowPlaceInfoWindow: onShowPlaceInfoWindow,
        offShowPlaceInfoWindow: offShowPlaceInfoWindow,
        findPath: findPath,
        fillingByClick: fillingByClick,
        setGeocoder: setGeocoder,
        defineMarker: defineMarker,
        defineCurrentPosition: defineCurrentPosition
    };

})();