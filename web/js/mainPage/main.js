//создание карты
google.maps.event.addDomListener(window, 'load', mapCtrl.init);

window.onload = function(){
    requestsCtrl.getShowPlaces();
    mapCtrl.setGeocoder(geocoderObj());
    mapCtrl.defineCurrentPosition();
    pageCtrl.initPageEvents();
};
 
 
 
 
$(".tooltip").mousemove(function (eventObject) {
		
	var mouseY = eventObject.pageY - 25;		
	var mouseX = eventObject.pageX + 5;						
	$("#tooltip").css(
	{
		"display" : "block",
		"top" : mouseY,
		"left" : mouseX
	});
	$("#tooltip").text($(this).attr("title"));	

	}).mouseout(function(){
		$("#tooltip").css("display","none");	
});