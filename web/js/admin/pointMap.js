(function ($) {

        var pointCoords = {
            lat: $('.point-lat').val(),
            lon: $('.point-lon').val()
        };

        function initPointMap() {
            console.log(3);
            var map = new google.maps.Map(document.getElementById('point-map'), {
                center: new google.maps.LatLng(pointCoords.lat, pointCoords.lon),
                zoom: 15,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.SMALL,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                scaleControlOptions: {
                    style: google.maps.ScaleControlStyle.SMALL,
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                mapTypeControl: false,
                draggableCursor: 'default',
                draggingCursor: 'move',
                disableDoubleClickZoom: true
            });

            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(pointCoords.lat, pointCoords.lon),
                map: map
            });

        }

        google.maps.event.addDomListener(window, 'load', initPointMap);
})(jQuery);