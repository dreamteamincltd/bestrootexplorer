<h1><?=$data['message']?></h1>

<table class="pointList">
<tr>
	<th>Название</th>
	<th></th>
	<th></th>
	<th>Lat</th>
	<th>Lon</th>
	<th>Описание</th>
	<th>Картинка</th>
</tr>
<?php
foreach ($data['points'] as $one)
{
	?>
	<tr>
		<td><?=$one['name']?></td>
		<td><a href='/admin/point?id=<?=$one['id']?>'>Редактировать</a></td>
		<td><a href="/admin/delete?id=<?=$one['id']?>" target="_blank" onClick="return window.confirm('Удалить точку <?=$one['name']?>?')">Удалить</a></td>
		<td><?=$one['lat']?></td>
		<td><?=$one['lon']?></td>
		<td><?=($one['description'] != NULL ? "+" : "-")?></td>
		<td><?=($one['img'] != NULL ? "+" : "-")?></td>
	</tr>
	<?php
}
?>
</table>