    <header class="content-header show">
    <nav class="tp-nav">
        <div class="tp-head-block">
            <figure class="tp-logo">
                <a href="/"><img src="/img/logo.png" alt="TravelPath"/></a>
            </figure>

            <div class="menu-block">
                <ul class="tp-navMenu">
                    <li><a class="set-route active" data-name="route-link" data-block-class="route-block" href="#">Маршруты</a></li>
                    <li><a class="about" data-name="about-link" data-block-class="about-block" href="#">О проекте</a></li>
                    <!--<li><a href="/admin"><span>admin</span></a></li>-->
                </ul>
                <span class="menu-arrow menu-arrow-show"></span>
            </div>
        </div>

        <div class="tp-content-block">
            <div class="route-block" >
                <form class="tp-navForm" name="navForm"  action="" method="POST">
                    <div class="selectPath">
                        <div class="from-fields" style="width: 100%">
                            <span class="nav-btn tooltip" title="Поставить ваше текущее местоположение"></span>
                            <input class="tp-fromPlace tp-inputPlace" type="text" placeholder="От"/>
                            <span class="clear-btn"></span>
                            <input class="tp-fromCoords" hidden="hidden" name="from" />
                        </div>
                        <div class="to-fields" style="width: 100%">
                            <span class="nav-btn tooltip" title="Поставить ваше текущее местоположение"></span>
                            <input class="tp-toPlace tp-inputPlace" type="text" placeholder="До" />
                            <span class="clear-btn"></span>
                            <input class="tp-toCoords" hidden="hidden" name="to" />
                        </div>
                    </div>

                    <ul class="selectPlace">
                        <li>
                            <label for="mus-check" title="Музеи" class="tooltip">
                                <input class="tp-typeSP" type="checkbox" checked="checked" name="museum" id="mus-check"/>
                                <span class="mus-icon check-icon"></span>
                            </label>
                        </li>
                        <li>
                            <label for="park-check" title="Парки" class="tooltip">
                                <input class="tp-typeSP" type="checkbox" checked="checked" name="park" id="park-check"/>
                                <span class="park-icon check-icon"></span>
                            </label>
                        </li>
                        <li>
                            <label for="mon-check" title="Памятники" class="tooltip">
                                <input class="tp-typeSP" type="checkbox" checked="checked" name="monument" id="mon-check"/>
                                <span class="mon-icon check-icon"></span>
                            </label>
                        </li>
                        <li>
                            <label for="church-check" title="Церкви" class="tooltip">
                                <input class="tp-typeSP" type="checkbox" checked="checked" name="church" id="church-check"/>
                                <span class="church-icon check-icon"></span>
                            </label>
                        </li>
                    </ul>

                    <div class="tp-findButton-container">
                        <button class="tp-findButton">Поиск</button>
                    </div>
                </form>

                <section class="tp-showPlaces">
                    <div class="list-container" style="direction: ltr">
                        <ul class="showPlacesList" style="direction: ltr">

                        </ul>
                    </div>
                </section>
            </div>

            <div class="about-block" style="display: none;">
                <div class="tp-about-text">
					<p>Travel Path - сервис для автоматического построения туристических маршрутов Санкт-Петербурга.</p>
					<p>В нашей базе наиболее популярные музеи, памятники, парки и церкви, через которые будет построен ваш маршрут.</p>
                    <p>Если у Вас появились какие-либо вопросы, воспользуйтесь формой обратной связи, и мы обязательно ответим Вам!</p>
                </div>

                <div class="tp-response">
                    <div class="response-title">Обратная связь</div>
                    <form class="response-form" name="responseForm" action='' method="POST">
                        <div class="form-field-container"><input type="text" name="responseName" placeholder="Имя" required="required" /></div>
                        <div class="form-field-container"><input type="email" name="responseEmail" placeholder="E-mail" required="required"/></div>
                        <div class="form-field-container"><textarea name="responseMessage" placeholder="Отзыв" required="required"></textarea></div>
                        <div class="response-btn-container">
                            <button class="response-btn">Отправить</button>
                            <div class="success-window">Отзыв успешно отправлен</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </nav>
    </header>

    <main>
        <div class="version">Version: <?=$data['version']?></div>

        <section id="tp-map"></section>
    </main>

<!----------------------------begin scripts------------------------------->

    <script src="http://yastatic.net/jquery/2.1.1/jquery.min.js"></script>
    <script src="http://cdn.jsdelivr.net/jquery.validation/1.13.1/jquery.validate.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="/js/mainPage/map.js"></script>
    <script src="/js/mainPage/interaction.js"></script>
    <script src="/js/mainPage/requests.js"></script>
    <script src="/js/mainPage/pageViewGenerator.js"></script>
    <script src="/js/mainPage/pageCtrl.js"></script>
    <script src="/js/mainPage/geocoder.js"></script>
    <script src="/js/mainPage/main.js"></script>
    <script src="/js/mainPage/pathFinder.js"></script>

<!-----------------------------end scripts--------------------------->