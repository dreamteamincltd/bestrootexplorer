<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <title><?=$data['title']?></title>
    <link rel="stylesheet" type="text/css" href="/css/admin-style.css">
	<link href="/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
</head>
<body>

<div class="wrapper">
    <?php include_once 'application/views/'.$content_view; ?>
</div>
</body>
</html>

