<h1><?=$data['title']?></h1>


<?php
if ($data['flag'])
{
?>
<div class="point-view-container">
    <div class="point-form-container">
        <form method="POST" class="point-form" enctype="multipart/form-data">
            <p>
                Lat:<br>
                <input class="point-lat" type="text" name="lat" value="<?=$data['point']['lat']?>">
            </p>
            <p>
                Lon:<br>
                <input class="point-lon" type="text" name="lon" value="<?=$data['point']['lon']?>">
            </p>
            <p>
                Имя:<br>
                <input type="text" name="name" value="<?=$data['point']['name']?>">
            </p>
            <p>
                Описание:<br>
                <textarea name="description"><?=$data['point']['description']?></textarea>
            </p>
            <p>
                Изображение:<br>
                <?php if ($data['point']['img'] != NULL)
                { ?>
                    <img src="<?=$data['point']['img']?>" alt="">
                <?php } ?>
                <input type="file" name="img">

            </p>
            <input type="hidden" name="fl" value="true">
            <input type="submit" value="Отправить">
        </form>
    </div>

    <div class="point-map-container">
        <div id="point-map" ></div>
    </div>

    <div class="point-clearfix"></div>
</div>

<!-----------------------------------begin scripts------------------------------>

    <script src="http://yastatic.net/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script src="/js/admin/pointMap.js"></script>

<!-----------------------------------end script--------------------------------->
<?php
}
else
{
	echo $data['errorMessage'];
}

?>
