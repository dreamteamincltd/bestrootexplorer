<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

    <title><?=$data['title']?></title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
	<link href="/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
	<meta property="vk:image" content="http://travelpath.ru/img/logo.jpg" />
	<meta property="og:image" content="http://travelpath.ru/img/logo.jpg" />
	<meta name="vk:image" content="http://travelpath.ru/img/logo.jpg" />
	<meta name="og:image" content="http://travelpath.ru/img/logo.jpg" />
</head>
<body>

    <?php include_once 'application/views/'.$content_view; ?>
	
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter27664326 = new Ya.Metrika({id:27664326,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/27664326" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<div id="tooltip"></div>
</body>
</html>

