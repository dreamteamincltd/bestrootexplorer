<?php
class Controller_Errors extends Controller
{
	function __construct()
    {
        $this->model = new Model_Errors();
        $this->view = new View();
    }
	
    function action_index()
    {	
        $data = $this -> model -> get_data();
        $this -> view -> generate('main_view.php', 'template_view.php', $data);
    }
	
	function action_404()
    {	
        $data = $this -> model -> get_404_data();
        $this -> view -> generate('404_view.php', 'template_view.php', $data);
    }
	

}