<?php
// контроллер для административной части - управление точками
class Controller_Admin extends Controller
{
	function __construct()
    {
        $this->model = new Model_Admin();
        $this->view = new View();
    }
	
	// страница списка точек
    function action_index()
    {		
        $data = $this -> model -> get_data();
        $this -> view -> generate('admin_view.php', 'admin_template_view.php', $data);
    }
	
	// страница с формой редактирования каждой точки
	function action_point()
    {		
        $data = $this -> model -> get_point();
        $this -> view -> generate('admin_point_view.php', 'admin_template_view.php', $data);
    }
	function action_delete()
    {		
        $data = $this -> model -> delete_point();
        $this -> view -> generate('admin_delete_view.php', 'admin_template_view.php', $data);
    }
	function action_login()
    {		
        $data = $this -> model -> login();
        $this -> view -> generate('admin_login_view.php', 'admin_template_view.php', $data);
    }
}