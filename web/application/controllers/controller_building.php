<?php
/*
	Контроллер, в котором будет расчёт всех возможных путей, поиск всех возмодных пар, тестирование этого
*/
class Controller_Building extends Controller
{
	function __construct()
    {
        $this->model = new Model_Building();
        $this->view = new View();
    }
	
    function action_index()
    {		
        $data = $this -> model -> get_data();
        $this -> view -> generateAjax('building_view.php', $data);
    }
	
	function action_allpair()
	{
		// рассчитывает все возможные пути между достопримечательностями (пары точек)
		$data = $this -> model -> get_allPair();
        $this -> view -> generateAjax('building_view.php', $data);
	}
	function action_testallpair()
	{
		// рассчитываем все возможные пути между достопримечательностями перебором (пары точек)
		$data = $this -> model -> get_testAllPair();
        $this -> view -> generateAjax('building_view.php', $data);
	}
	
	function action_buildroute()
	{
		// строим маршрут
		$data = $this -> model -> buildRoute();
        $this -> view -> generateAjax('building_view.php', $data);
	}
}