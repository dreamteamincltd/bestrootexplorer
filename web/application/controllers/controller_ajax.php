<?php
class Controller_Ajax extends Controller
{
	function __construct()
    {
        $this->model = new Model_Ajax();
        $this->view = new View();
    }
	
    function action_index()
    {		
        $data = $this -> model -> get_data();
        $this -> view -> generateAjax('ajax_view.php', $data);
    }
	
	function action_way()
	{
		$data = $this -> model -> get_way();
		$this -> view -> generateAjax('ajax_view.php', $data);
	}
	
	function action_mail()
	{
		$data = $this -> model -> get_mail();
		$this -> view -> generateAjax('ajax_view.php', $data);
	}
}