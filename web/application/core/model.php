<?php
class Model
{
	public $base;
	public $email;
	public $version;
	public $admin_password;
	
	function __construct()
	{ 
		session_start();
		
		// получаем версию сборки
		$this -> version = file_get_contents($_SERVER['DOCUMENT_ROOT'].'/application/core/version.txt');
		$this -> email = MyConfig::$email;
		$this -> admin_password = MyConfig::$admin_password;
		$this -> base = new Base;
	}
    public function get_data()
    {
    }
}