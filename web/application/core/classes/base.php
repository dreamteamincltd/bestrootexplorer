<?php
class Base {    
    private $base;
	private $host;
	private $baseName;
	private $user;
	private $password;
	
    function __construct()
    {		
		$this -> host = MyConfig::$host;
		$this -> baseName = MyConfig::$base_name;
		$this -> user = MyConfig::$base_user;
		$this -> password = MyConfig::$base_password;
		
		$this -> base = new PDO("mysql:host=".$this->host.";dbname=".$this->baseName, $this->user, $this->password); 
		$this -> base -> query("set names utf8");		 
    }  
	

	
	// при запросе без последних параметров, вернёт просто всё что есть (ибо точек не будет больше чем пара тысячей)
	function get_points($tablename, $limit_s = 0, $limit_n = 1000000, $order = false)
	{		
		$sql = 'SELECT * FROM '.$tablename.' ';
		if ($order == 'rand')
		{
			$sql .= 'ORDER BY RAND() ';
		}
		$sql .= 'LIMIT :limit_s, :limit_n';
		$sql = $this -> base -> prepare($sql);
		$sql -> bindParam (':limit_s',$limit_s,PDO::PARAM_INT);
		$sql -> bindParam (':limit_n',$limit_n,PDO::PARAM_INT);
		$sql -> execute();
		$ret = $sql -> fetchAll();
			
		return $ret;		
	}
	
	function get_point($tablename, $id)
	{
		$sql = 'SELECT * FROM '.$tablename.' WHERE id = :id';
		$sql = $this -> base -> prepare($sql);
		$sql -> bindParam (':id',$id,PDO::PARAM_INT);
		$sql -> execute();
		$ret = $sql -> fetch();
		return $ret;	
	}
	
	function update_point($tablename, $id,  $lat, $lon, $name, $description, $img)
	{
		$sql = "
		UPDATE ".$tablename." 
		SET
			lat = :lat,
			lon = :lon,
			name = :name,
			description = :description,
			img = :img
		WHERE 
			id = :id";
		$sql = $this->base -> prepare($sql);
		$sql -> bindParam (':id',$id,PDO::PARAM_INT);
		$sql -> bindParam (':lat',$lat,PDO::PARAM_STR);
		$sql -> bindParam (':lon',$lon,PDO::PARAM_STR);
		$sql -> bindParam (':name',$name,PDO::PARAM_STR);
		$sql -> bindParam (':description',$description,PDO::PARAM_STR);
		$sql -> bindParam (':img',$img,PDO::PARAM_STR);
		$fl = $sql -> execute();
		return $fl;			
	}
	
	// находим все точки в окружности
	function get_points_radius ($tablename, $point, $radius, $types = false)
	{
		if ($types == false or sizeof($types) == 0)
		{
			$sql = 'SELECT * FROM '.$tablename.' WHERE POW(? - lat,2) + POW(? - lon,2) <= POW(?,2) LIMIT 500';
		}
		else
		{
			$typesQ = implode(',', array_fill(0, count($types), '?'));
			$sql = 'SELECT * FROM '.$tablename.' WHERE POW(? - lat,2) + POW(? - lon,2) <= POW(?,2) AND type IN ('.$typesQ.')  LIMIT 500';
		}
		$sql = $this -> base -> prepare($sql);
		$radius = $radius * $radius;
		$sql -> bindParam (1, $point['lat'], PDO::PARAM_STR);
		$sql -> bindParam (2, $point['lon'], PDO::PARAM_STR);
		$sql -> bindParam (3,$radius,PDO::PARAM_STR);
		
		if ($types != false AND sizeof($types) != 0)
		{	
			foreach ($types as $k => $id)
			{
				$sql -> bindValue(($k+4), $id);
			}
		}		
		$sql -> execute();
		$ret = $sql -> fetchAll();
		return $ret;		
	}
	
	// находим все точки в квадрате
	function get_points_square ($a, $b, $types = false)
	{
		$distanse = distance_hypot($a, $b) / 5;		// расширяем прямоугольник на пятую часть гипотинузы 
		$lat_min = min($a['lat'], $b['lat']) - $distanse;
		$lat_max = max($a['lat'], $b['lat']) + $distanse;
		$lon_min = min($a['lon'], $b['lon']) - $distanse;
		$lon_max = max($a['lon'], $b['lon']) + $distanse;
		if ($types == false or sizeof($types) == 0)
		{
			$sql = 'SELECT * FROM points WHERE lat < ? and lat > ? and lon < ? and lon > ? LIMIT 500';
		}
		else
		{
			$typesQ = implode(',', array_fill(0, count($types), '?'));
			$sql = 'SELECT * FROM points WHERE lat < ? and lat > ? and lon < ? and lon > ? AND type IN ('.$typesQ.')  LIMIT 500';
		}
		$sql = $this -> base -> prepare($sql);
		$sql -> bindParam (1, $lat_max, PDO::PARAM_STR);
		$sql -> bindParam (2, $lat_min, PDO::PARAM_STR);
		$sql -> bindParam (3, $lon_max, PDO::PARAM_STR);
		$sql -> bindParam (4, $lon_min, PDO::PARAM_STR);
		if ($types != false AND sizeof($types) != 0)
		{	
			foreach ($types as $k => $id)
			{
				$sql -> bindValue(($k+5), $id);
			}
		}		
		$sql -> execute();
		$ret = $sql -> fetchAll();
		return $ret;		
	}
	

	function delete_point($tablename, $id)
	{
		$sql = "DELETE FROM ".$tablename." WHERE id = :id";
		$sql = $this->base -> prepare($sql);
		$sql -> bindParam (':id',$id,PDO::PARAM_INT);
		$fl = $sql -> execute();
		return $fl;			
	}
	
	// добавляем в базу тест пары
	function add_test_pairs($pairs) {	
		$sql = 'INSERT INTO test_pairs (id_point1, id_point2) VALUES ';		
		$new_q = true;
		$i = 0;
		$n = sizeof($pairs);
		foreach ($pairs as $key => $one) {
			if ($new_q) {
				$sql_q = $sql;
				$new_q = false;
			}
			
			$sql_q .= "('".$one[0]."', '".$one[1]."')";
			
			if ($n - 1 == $key or $i > 1000) {		// за 1 запрос не больше 1000 добавлений
				$i = 0;
				$sql_q .= ';';
				$new_q = true;
				$prepare = $this -> base -> prepare($sql_q);
				$fl = $prepare -> execute();	
				if ($fl == false) {
					var_dump('ERROR', $sql_q);
					exit();
				}
			}
			else {
				$sql_q .= ', ';
			}
			$i++;
		}
	}
}