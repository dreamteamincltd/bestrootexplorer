<?php
class routeBuilder
{
	private $point_from;			// array('lat' => '', 'lon'=>'');
	private $point_to;				// array('lat' => '', 'lon'=>'');
	private $points;
	private $ways;
	private $route;
	private $route_ready;	// переменная, в зависимости от которой при вызове метода get_route() будет либо строиться новый маршрут (если данные были изменены или маршрут ещё не был построен), либо возвращаться старый
	
	function __construct($a = NULL, $b = NULL) {
		$this->point_from = $a;
		$this->point_to = $b;
		$this->route_ready = false;		
	}
	
	public function set_from($point) {
		$this->point_from = $point;
		$this->route_ready = false;
	}	
	public function set_to($point) {
		$this->point_to = $point;
		$this->route_ready = false;
	}	
	public function get_from() {
		return $this->point_from;
	}
	public function get_to() {
		return $this->point_to;
	} 	
	public function get_route() {	

		if (!$this->route_ready) 
		{
			$this->build();
		}
		return $route;
	}
	
	private function build () {
	
	
	
		$this->route_ready = true;
	}
};

?>