<?php
function translit($filename) 
{ 
	$transsimvol = array( 
	"А"=>"a","Б"=>"b","В"=>"v","Г"=>"g", 
	"Д"=>"d","Е"=>"e","Ж"=>"zh","З"=>"z","И"=>"i", 
	"Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n", 
	"О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t", 
	"У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch", 
	"Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"y","Ь"=>"", 
	"Э"=>"e","Ю"=>"yu","Я"=>"ya", 
	"а"=>"a","б"=>"b","в"=>"v","г"=>"g","д"=>"d", 
	"е"=>"e","ж"=>"j","з"=>"z","и"=>"i","й"=>"y", 
	"к"=>"k","л"=>"l","м"=>"m","н"=>"n","о"=>"o", 
	"п"=>"p","р"=>"r","с"=>"s","т"=>"t","у"=>"u", 
	"ф"=>"f","х"=>"h","ц"=>"ts","ч"=>"ch","ш"=>"sh", 
	"щ"=>"sch","ъ"=>"","ы"=>"y","ь"=>"","э"=>"e", 
	"ю"=>"yu","я"=>"ya",
	" "=>"_"
	); 
	return strtr($filename,$transsimvol); 
}  

function sendmail($to, $from, $subject, $message) 
{ 
	$mailheaders = "Content-type:text/html; charset=utf-8\r\n"; 
//	$name =  base64_encode(iconv('UTF-8', 'CP1251', $name));		для русских заголовков
//	$mailheaders .= "From: =?CP1251?B?".$name."?= <noreply@".$_SERVER['HTTP_HOST'].">\r\n"; 
	$mailheaders .= "From: <".$from.">\r\n"; 
	$mailheaders .= "Reply-To: ".$from."\r\n"; 
	mail($to, $subject, $message, $mailheaders);
}  


// Генерация случайной строки
function random_string($length) {
   $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
   $chars_length = (strlen($chars) - 1);   
   $string = $chars{rand(0, $chars_length)};   
	for ($i = 1; $i < $length; $i = strlen($string))
	{
		$random = $chars{rand(0, $chars_length)};
		if ($random != $string{$i - 1}) $string .= $random;
	}
	return $string;
}


// дистанция между точками (простая гипотенуза)
function distance_hypot($a, $b)
{
	$len = false;
	if (isset($a['lat']) and isset($a['lon']) and isset($b['lat']) and isset($b['lon']))
	{
		$len = hypot($a['lat'] - $b['lat'], $a['lon'] - $b['lon']);
	}
	return $len;
} 
// дистанция между точками в метрах
function distance($a, $b)
{
	//радиус Земли
    $R = 6372795;
    
	$lat1 = $a['lat'];
	$lat2 = $b['lat'];
	$long1 = $a['lon'];
	$long2 = $b['lon'];
	
    //перевод коордитат в радианы
    $lat1 *= pi() / 180;
    $lat2 *= pi() / 180;
    $long1 *= pi() / 180;
    $long2 *= pi() / 180;
     
    //вычисление косинусов и синусов широт и разницы долгот
    $cl1 = cos($lat1);
    $cl2 = cos($lat2);
    $sl1 = sin($lat1);
    $sl2 = sin($lat2);
    $delta = $long2 - $long1;
    $cdelta = cos($delta);
    $sdelta = sin($delta);
     
    //вычисления длины большого круга
    $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
    $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;
    $ad = atan2($y, $x);
    $dist = $ad * $R; //расстояние между двумя координатами в метрах
 
    return $dist;
}

// отрицательная/положительная дистанция,  недокосинус
function dist_cos($from, $point, $A, $B){	
	$prod = ($point['lat'] - $from['lat']) * ($B['lat'] - $A['lat'])
	+ ($point['lon'] - $from['lon']) * ($B['lon'] - $A['lon']);	
	if ($prod >= 0){
		$prod = distance($from, $point);
	}
	return $prod;
}

// дистанция от точки до прямой
function dist_to_AB($point, $coeffs, $lenAB){
	$res = ($coeffs['a'] * $point['lat'] + $coeffs['b'] * $point['lon'] + $coeffs['c']) / $lenAB;
	return abs($res);
}

// преобразует формат точек из базы под формат отображения на карту
function prepareData($mass)
{
	$data = array();
	foreach ($mass as $key => $one)
	{
		if (!isset($one['img']))
		{
			$one['img'] = NULL;
		}
		if (!isset($one['type']))
		{
			$one['type'] = NULL;
		}
		if (!isset($one['description']))
		{
			$one['description'] = '';
		}
		$data[] = array(
			"point" => array(
				"lat" => $one['lat'],
				"lon" => $one['lon']
			),
			"describe" => array(
				"img" => $one['img'],
				"text" => $one['name'],
				"description" => $one['description'],
				"type" => $one['type'],
			)
		);
	}
	$data = json_encode($data);	
	
	return $data;
}

// возвращает массив с набором точек - путь от А до Б через достопримечательности, выбранные из $points
function getWayObjects($A, $B, $points)
{
	$data = array();
	$from = $A;
	$data[] = $from;	
	$coeffs = array(
		'a' => $A['lon'] - $B['lon'],
		'b' => $B['lat'] - $A['lat'],
		'c' => $A['lat'] * $B['lon'] - $B['lat'] * $A['lon'],
	);		
	$lenAB = distance($A, $B);	
	$dist = 0;		
	while (!empty($points)){
		$tmpDist = 0;
		$dist = 0;
		$index = 0;
		$isAdded = False;
		$sizePoints = count($points);
		for ($i = 0; $i < $sizePoints; ++$i){
			$tmpDist = dist_cos($from, $points[$i], $A, $B); // расстояние от пред. точки до следующей
			if ($tmpDist > 0){			
				$tmpDist += 4 * dist_to_AB($points[$i], $coeffs, $lenAB); // tmpDist + 4 * расстояние от точки до прямой
				if (!$isAdded)	// выбираем первое место
					$isAdded = True;
				else if ($tmpDist >= $dist)
					continue;
				$index = $i;	// ищем минимум
				$dist = $tmpDist;
			}			
		}
		if (dist_cos($B, $points[$index], $B, $A) <= 0 || distance($A, $points[$index]) > $lenAB)
			break;
		$from = $points[$index];
		$data[] = $from;
		array_splice($points, $index, 1);
	}	
	
	$data[] = $B;
	
	return $data;
}
