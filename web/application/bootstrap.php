<?php
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/route.php';
require_once 'core/functions.php';
require_once 'core/classes/base.php';
require_once 'core/classes/routeBuild.php';
require_once 'core/config.php';

Route::start(); // запускаем маршрутизатор