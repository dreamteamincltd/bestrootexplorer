<?php
class Model_Errors extends Model
{
    public function get_data()
    {				
		$data['title'] = "Ошибка";
		$data['message'] = "Ошибка";
		return $data;	
    }
	
    public function get_404_data()
    {				
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
		$data['title'] = "404 Ошибка";
		$data['message'] = "Страница удалена или никогда не существовала";
		return $data;	
    }
}