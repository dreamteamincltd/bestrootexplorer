<?php
// модель для административной части - управление точками
class Model_Admin extends Model
{
    public function get_data()
    {		
		if (!isset($_SESSION['login']))
		{
			Route::ErrorAccessAdmin();
		}
		
		$data['points'] = $this->base->get_points('points');
		$data['title'] = "Список всех точек";
		$data['message'] = "Список всех точек";
		return $data;
		
    }
	
	public function get_point()
	{
		if (!isset($_SESSION['login']))
		{
			Route::ErrorAccessAdmin();
		}
		
		$data['id'] = isset($_GET['id']) ? $_GET['id'] : NULL;
		
		$data['flag'] = false; // если он false - значит id неверный 
		if ($data['id'] != NULL and is_numeric($data['id']))
		{
			$data['point'] = $this -> base -> get_point('points', $data['id']);
			
			if ($data['point'] != false)
			{
				
				$data['flag'] = true;	
		
				// делаем что бы нельзя было удалять данные, на всякий случай (т.е. если поле остаётся пустым - в базу запишется предыдущее его значение)
				$point['lat'] = isset($_POST['lat']) ? $_POST['lat'] : $data['point']['lat'];
				$point['lon'] = isset($_POST['lon']) ? $_POST['lon'] : $data['point']['lon'];
				$point['name'] = isset($_POST['name']) ? $_POST['name'] : $data['point']['name'];
				$point['description'] = isset($_POST['description']) ? $_POST['description'] : $data['point']['description'];
				$point['flag'] = isset($_POST['fl']) ? $_POST['fl'] : false;
				
				
				$point['img'] = $data['point']['img'];
				
				if ($point['flag'])
				{	
					// занимаемся загрузкой файла
					if (isset($_FILES['img']))
					{
						$format = explode('.', $_FILES['img']['name']);
						$format = array_pop($format);

						// что бы имя было уникальным 		
						$name = rand(1000, 9999).'_'.md5($_FILES['img']['name']);	
						
						// записываем путь до новой картинки 
						$image_way = "/points/" . $name . "." . $format;		
						
						$name =  $_SERVER['DOCUMENT_ROOT'] . "/points/" . $name . "." . $format;
											
						$fileUpdate = move_uploaded_file($_FILES['img']['tmp_name'], $name);
						
						if ($fileUpdate)
						{
							$point['img'] = $image_way;		
						}
					}
					
					// значит форма отправилась и мы должно обновить данные о точке
					$fl = $this -> base -> update_point('points', $data['id'], $point['lat'], $point['lon'], $point['name'], $point['description'], $point['img']);
					if ($fl)
					{
						// обновляем данные, которые отображаются на странице
						$data['point'] = $point;
					}
				}
				
			}
		}
		
		$data['title'] = "Редактирование точки";
		$data['errorMessage'] = "Ошибка!";
		return $data;
	}
	
	public function delete_point()
	{
		if (!isset($_SESSION['login']))
		{
			Route::ErrorAccessAdmin();
		}
		
		$data['id'] = isset($_GET['id']) ? $_GET['id'] : NULL;
		
		$data['flag'] = false; // если он false - значит id неверный 
		
		if ($data['id'] != NULL and is_numeric($data['id']))
		{
			$deleteFlag = $this -> base -> delete_point('points', $data['id']);
			
			if ($deleteFlag)
			{
				$data['message'] = 'Точка удалена';				
			}
			else
			{
				$data['message'] = 'Точка не найдена';
			}
		}
		else
		{
			$data['message'] = 'Неверное значение id';
		}
		
		$data['title'] = "Удаление точки";
		return $data;
	}
	
	public function login()
	{
		if (isset($_SESSION['login']))
		{
			Route::SuccessLoginAdmin();
		}
		
		$data['pass'] = isset($_POST['pass']) ? $_POST['pass'] : NULL;
		$data['Message'] = "Введите пароль для доступа";
		if ($data['pass'] != NULL)
		{
			if ($data['pass'] == $this -> admin_password)
			{
				$_SESSION['login'] = true;
				$data['Message'] = "Авторизация успешна";
				Route::SuccessLoginAdmin();
			}
			else
			{
				$data['Message'] = "Неверный пароль";
			}
		}
		$data['title'] = "Авторизация";
		return $data;
	}
}