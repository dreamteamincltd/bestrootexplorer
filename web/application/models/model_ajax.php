<?php
class Model_Ajax extends Model
{
    public function get_data()
    {				
		// http://bestrootexplorer.dev/ajax/
		
		
		$points = $this -> base -> get_points('points', 0, 50, 'rand');
		$data = prepareData($points);

		
	/*	
		test
		$points = $this -> base -> get_points('points');
		$A = array(
			'lat' => 59.937015,
			'lon' => 30.312582,
			'name' => 'from',
		);		
		$B = array(
			'lat' => 59.931049,
			'lon' => 30.360690,		
			'name' => 'to',
		);				
		
		// вычисляем центр нашего маршрута и его радиус, чтобы в алгоритм ставить только точки, входящие в этот радиус, а не все подряд 
		$middle = array(
		 'lat' => ($A['lat'] + $B['lat']) / 2,
		 'lon' => ($A['lon'] + $B['lon']) / 2,
		 );
		$r = distance_hypot($A, $B) * 1.5;				
		$r = $r * 3; 	// магическое число, чтобы точки выводились. нужно разобраться что за фигня и почему не ищет просто по радиусу
		$points = $this -> base -> get_points_radius ('points', $middle, $r);
		$points = $this -> base -> get_points_square ($A, $B);
	//	var_dump($points);
	//	exit;
		$data = prepareData($points);
	*/
	
        //test
        /*$data = array(
            "point" => array(
                "lat" => 1,
                "lon" => 1
            ),
            "describe" => array(
                "img" => "img/showPlaces/isaac.jpg",
                "text" => 'dslfjklsdjljsdkl'
            )
        );*/


		return $data;
    }
	
	public function get_way()
	{
		// http://bestrootexplorer.dev/ajax/way	
		
		$data['from'] = isset($_POST['from']) ? $_POST['from'] : NULL;
		$data['to'] = isset($_POST['to']) ? $_POST['to'] : NULL;
		
		
		$data['monument'] = isset($_POST['monument']) ? $_POST['monument'] : NULL;
		$data['museum'] = isset($_POST['museum']) ? $_POST['museum'] : NULL;
		$data['park'] = isset($_POST['park']) ? $_POST['park'] : NULL;
		$data['church'] = isset($_POST['church']) ? $_POST['church'] : NULL;
		
		
		// получаем какие пункты отмечены
		$types = array();
		if ($data['monument'] == 'on')
		{
			$types[] = "monument";
		}
		if ($data['museum'] == 'on')
		{
			$types[] = "museum";
		}
		if ($data['park'] == 'on')
		{
			$types[] = "park";
		}
		if ($data['church'] == 'on')
		{
			$types[] = "church";
		}
			
//		$data['from'] = '59.97563121816459, 30.310592651367188';  /////////////
//		$data['to'] = '59.86050557093743, 30.34046173095703';  ////////////
		
		$data['from'] = explode(', ', $data['from']);
		$data['to'] = explode(', ', $data['to']);
		
		$A = array(
			'lat' => $data['from'][0],
			'lon' => $data['from'][1],
			'name' => 'from',
		);		
		$B = array(
			'lat' => $data['to'][0],
			'lon' => $data['to'][1],		
			'name' => 'to',
		);				
		
		// вычисляем центр нашего маршрута и его радиус, чтобы в алгоритм ставить только точки, входящие в этот радиус, а не все подряд 
	//	 $middle = array(
	//	 'lat' => ($A['lat'] + $B['lat']) / 2,
	//	 'lon' => ($A['lon'] + $B['lon']) / 2,
	//	 );
	//	$r = distance_hypot($A, $B) * 1.5;				
	//	$r = $r * 5; 	// магическое число, чтобы точки выводились. нужно разобраться что за фигня и почему не ищет просто по радиусу
	//	$points = $this -> base -> get_points_radius ('points', $middle, $r, $types);
	//	$points = $this -> base -> get_points('points');
		$points = $this -> base -> get_points_square($A, $B);
		
		$data = getWayObjects($A, $B, $points);
		$data = prepareData($data);
		return $data;		
	}

	public function get_mail()
	{		
		$dataForm['responseName'] = isset($_POST['responseName']) ? $_POST['responseName'] : NULL;
		$dataForm['responseEmail'] = isset($_POST['responseEmail']) ? $_POST['responseEmail'] : NULL;
		$dataForm['responseMessage'] = isset($_POST['responseMessage']) ? $_POST['responseMessage'] : NULL;
		$data = 'error';
		if ($dataForm['responseName'] != NULL and $dataForm['responseEmail'] != NULL and $dataForm['responseMessage'] != NULL)
		{
			$subject = 'Отзыв';
			$message = 'Имя: '.$dataForm['responseName'].'<br>Сообщение: '.$dataForm['responseMessage'];
			$message = str_replace("\n", "<br>", $message);
			$to = $this -> email;
			sendmail($to, $dataForm['responseEmail'], $subject, $message);
			$data = 'ok';
		}
		return $data;
	}
}