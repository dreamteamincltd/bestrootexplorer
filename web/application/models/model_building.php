<?php
class Model_Building extends Model
{
    public function get_data()
    {	
		$data = 'It work!';
		return $data;
    }
	
    public function get_allPair()
    {	
		$points = $this -> base -> get_points('points');
		
		
		$data = 'It work!';
		return $data;
    }
	
	// заполняем базу для тестирования всех возможных путей между достопримечательностями
	public function get_testAllPair() {	
	
		if (!isset($_GET['test']) or $_GET['test'] != 5)		// от случайного запуска
		{
			exit;
		}
		
		ini_set("memory_limit", "1024M");
		set_time_limit(0);
		
	// границы
	// lat:	60.5 -- 59.5
	// lon:	 29.2  --   31
		$num = 10000000;
		$lat_min = 59.5 * $num;
		$lat_max = 60.5 * $num;
		$lon_min = 29.2 * $num;
		$lon_max = 31 * $num;
		$data = array();
	//	$points = $this -> base -> get_points('points');
		$types_array = array(			
			array("monument"),
			array("museum"),
			array("park"),
			array("church"),
			array("monument", "museum"),
			array("monument", "park" ),
			array("monument", "church"),
			array("museum", "park" ),
			array("museum", "church"),
			array("park", "church"),
			array("monument", "museum", "park"),
			array("monument", "museum", "church"),
			array("monument",  "park", "church"),
			array("museum", "park", "church"),
			array("monument", "museum", "park", "church"),
		);
		foreach ($types_array as $types)
		{
			for ($j = 0; $j < 10000; $j++) 
			{
				$A = array(				
					'lat' => rand($lat_min, $lat_max)/ $num,
					'lon' => rand($lon_min, $lon_max)/ $num,
					'name' => 'from',
				);		
				$B = array(
					'lat' => rand($lat_min, $lat_max)/ $num,
					'lon' => rand($lon_min, $lon_max)/ $num,	
					'name' => 'to',
				);	
				$points = $this -> base -> get_points_square($A, $B, $types);
				$dataPoints = getWayPoints($A, $B, $points);
				
				// получаем пары всех достопримечательностей выбранного пути. 1 и посл. точки выкидываем - это нач и кон точка маршрута
				for ($i = 1; $i < sizeof($dataPoints) - 2; $i++)
				{	
					// для простоты первое значение меньшее, второе большее
					$data[] = array(min($dataPoints[$i]['id'], $dataPoints[$i+1]['id']), max($dataPoints[$i]['id'], $dataPoints[$i+1]['id']));
				}	
			}
			
			$data = array_unique($data, SORT_REGULAR); 			// флаг работает только с PHP версии 5.2.9
		}
		$this -> base -> add_test_pairs($data);
		$data = 0;
		/*
		foreach ($data as $one)
		{
		}
		*/
		return $data;
    }
	
	// построение маршрута
	public function buildRoute()
	{
		$data['from'] = '59.937029, 30.313282';  /////////////
		$data['to'] = '59.933356,30.342347';  ////////////
		
		$data['from'] = explode(',', $data['from']);
		$data['to'] = explode(',', $data['to']);
		
		$A = array(
			'lat' => trim($data['from'][0]),
			'lon' => trim($data['from'][1]),
			'name' => 'from',
		);		
		$B = array(
			'lat' => trim($data['to'][0]),
			'lon' => trim($data['to'][1]),		
			'name' => 'to',
		);
		
		$route = new routeBuilder($A, $B);
		$route -> get_route();
	//	$points = $this -> base -> get_points_square($A, $B);
		
		
		$data = $route;
		return $data;
	}
}