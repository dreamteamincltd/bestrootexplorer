<?php
exit;
set_time_limit(0); 
ini_set("memory_limit", "1024M");

include_once "functions.php";

$base = new Base('routemaps');

$highwayKeys = array(
	'motorway',
	'trunk',
	'primary',
	'secondary',
	'tertiary',
	'unclassified',
	'residential',
	'service',
	'motorway_link',
	'trunk_link',
	'primary_link',
	'secondary_link',
	'tertiary_link',
	'living_street',
	'pedestrian',
	'track',
	'road',
	'footway',
);

$reader = new XMLReader();

$reader->open("saint-petersburg_russia.osm");

$node = '';
$nodes = array();
$nodeOpen = false;

$iNodes = 0;
$iWays = 0;

$way = '';
$ways = array();
$wayOpen = false;
while ($reader->read()) {
	switch ($reader->nodeType) {
		case (XMLREADER::ELEMENT):
			if ($reader->localName == "node") 
			{
				if ($nodeOpen)
				{
				//	$nodes[] = $node;
				//	var_dump($node);
				//	file_put_contents('nodes.txt', var_dump_str($node), FILE_APPEND);
				
					addNodezz($base, $nodes, $node, $iNodes);
					
					$node = '';
					$way = '';
				}
				$nodeOpen = true;
				$wayOpen = false;
				$node['id'] = $reader->getAttribute("id");
				$node['lat'] = $reader->getAttribute("lat");
				$node['lon'] = $reader->getAttribute("lon");

				$node['tags'] = array();
			}
			
			if ($reader->localName == "way") 
			{
				if ($wayOpen)
				{
				//	$ways[] = $way;
				//	var_dump($way);
				//	file_put_contents('ways.txt', var_dump_str($way), FILE_APPEND);
					
					addWay($base, $ways, $way, $iWays, $highwayKeys);
					
					$way = '';
					$node = '';
				}
				$wayOpen = true;
				$nodeOpen = false;
				$way['id'] = $reader->getAttribute("id");
				
				$way['tags'] = array();	
				$way['nd'] = array();				
			}
			if ($reader->localName == "tag")
			{
				$key = $reader->getAttribute("k");
				$val = $reader->getAttribute("v");
				if ($nodeOpen)
					$node['tags'][$key] = $val;
				elseif($wayOpen)
					$way['tags'][$key] = $val;
			}
			
			if ($reader->localName == "nd")
			{
				$val = $reader->getAttribute("ref");
				if ($wayOpen)
				{
					$way['nd'][] = $val;
				/*	if(isset($way['id']))
					{
						$base -> addNd('nd', $way['id'], $val);
					}*/
				}
			}
			
			break;
			
		case (XMLREADER::END_ELEMENT):
			if ($reader->localName == "node") 
			{
				$nodeOpen = false;
				$wayOpen = false;
			//	$nodes[] = $node;
			//	var_dump($node);
			//	file_put_contents('nodes.txt', var_dump_str($node), FILE_APPEND);
				
				addNode($base, $nodes, $node, $iNodes);
					
				$node = '';
				$way = '';
			}
			if ($reader->localName == "way") 
			{
				$nodeOpen = false;
				$wayOpen = false;
			//	$ways[] = $way;
			//	var_dump($way);
			//	file_put_contents('ways.txt', var_dump_str($way), FILE_APPEND);
				
				addWay($base, $ways, $way, $iWays, $highwayKeys);
				
				$node = '';
				$way = '';
			}			
	}
}


addNode($base, $nodes, $node, $iNodes, true);
addWay($base, $ways, $way, $iWays, $highwayKeys, true);

//var_dump($nodes);
echo "<hr><hr><hr><hr>";
//var_dump($ways);