<?php

// работа с базой
class Base
{
	public $base;
	
	function __construct($bdname)
	 {
		$bdname = 'routemaps';
		$this -> base = new PDO("mysql:host=localhost;dbname=".$bdname, "root"); 
		$this -> base -> query("set names utf8");
	}
	
	function getPointsLocation($tablename, $id)
	{		
		$sql = 'SELECT lat, lon FROM '.$tablename.' WHERE id_node = :id';
		$sql = $this -> base -> prepare($sql);
		$sql -> bindParam (':id',$id);
		$fl = $sql -> execute();
		$nds = $sql -> fetchAll();
		return $nds;
	}
	
	function addNode($tablename, $id_node, $lat, $lon)
	{		
		$flag = false;
		$sql = 'INSERT INTO '.$tablename.' (id_node, lat, lon, flag) 
				VALUES (:id_node, :lat, :lon, :flag)';
		$sql = $this -> base -> prepare($sql);
		$sql -> bindParam (':id_node',$id_node);
		$sql -> bindParam (':lat',$lat);
		$sql -> bindParam (':lon',$lon);
		$sql -> bindParam ('flag',$flag);
		$fl = $sql -> execute();	
		
		if ($fl == false)
		{
			echo "ERROR INSERT\n<br>Table: ".$tablename;
			exit;
		}	
	}
	function addAllNode($tablename, $mass)
	{		
		$flag = false;
		$sql = 'INSERT INTO '.$tablename.' (id_node, lat, lon, flag) 
				VALUES ';
		$n = sizeof($mass);
		foreach ($mass as $key => $one)
		{
			
			$sql .= '(' . $one['id'] . ', ' . $one['lat'] . ', ' . $one['lon'] . ', 0)';
			if ($n - 1 == $key)
				$sql .= ';';
			else
				$sql .= ', ';
		}
		$sql = $this -> base -> prepare($sql);
		$fl = $sql -> execute();	
		
		if ($fl == false)
		{
			echo "ERROR INSERT\n<br>Table: ".$tablename;
			exit;
		}		
	}
	
	function addMassPoints($tablename, $mass)
	{
		$sql = 'INSERT INTO '.$tablename.' (lat, lon, type, name) 
				VALUES ';
		$n = sizeof($mass);
		foreach ($mass as $key => $one)
		{
			$name = $one['name'];
			$type =  $one['type'];
			$sql .= '(' . $one['lat'] . ', ' . $one['lon'] . ", '$type', '$name')";
			if ($n - 1 == $key)
				$sql .= ';';
			else
				$sql .= ', ';
		}
		
		$sql = $this -> base -> prepare($sql);
		$fl = $sql -> execute();	
		
		if ($fl == false)
		{
			echo "ERROR INSERT\n<br>Table: ".$tablename;
			exit;
		}
	}
	
	function addAllNodeWithoutFlag($tablename, $mass)
	{		
		$flag = false;
		$sql = 'INSERT INTO '.$tablename.' (id_node, lat, lon) 
				VALUES ';
		$n = sizeof($mass);
		foreach ($mass as $key => $one)
		{
			if ($one['flag'] == 1)
			{
				$sql .= '(' . $one['id_node'] . ', ' . $one['lat'] . ', ' . $one['lon'] . '), ';
			}
		}
		$sql[strlen($sql) - 2] = ';'; 
		$sql = $this -> base -> prepare($sql);
		$fl = $sql -> execute();	
		
		if ($fl == false)
		{
			echo "ERROR INSERT\n<br>Table: ".$tablename;
			exit;
		}		
	}
	
	function addAllWay($tablename, $mass)
	{		
		$sql = 'INSERT INTO '.$tablename.' (id_way, name, type) 
				VALUES ';
		$n = sizeof($mass);
		foreach ($mass as $key => $one)
		{			
			$id = $one['id'];
			$name = $one['tags']['name'];
			$h = $one['tags']['highway'];
			$sql .= "($id, '$name', '$h')";
			if ($n - 1 == $key)
				$sql .= ';';
			else
				$sql .= ', ';
		}
		$sql = $this -> base -> prepare($sql);
		$fl = $sql -> execute();	
		
		if ($fl == false)
		{
			echo "ERROR INSERT\n<br>Table: ".$tablename;
			exit;
		}		
	}
	function  addAllNd($tablename, $mass)
	{
		$sql = 'INSERT INTO '.$tablename.' (id_way, id_node) 
				VALUES ';
		$n = sizeof($mass);
		
		foreach ($mass as $key => $one)
		{
			$id_way = $one['id'];
			if (isset($one['nd']))
			{
				$n2 = sizeof($one['nd']);
				foreach ($one['nd'] as $key2 => $nds)
				{
					$id_node = $nds;
					$sql .= "($id_way, $id_node)";
					
					if ($n - 1 == $key and $n2 - 1 == $key2)
						$sql .= ';';
					else
						$sql .= ', ';
				}
			}
		}	
		$sql = $this -> base -> prepare($sql);
		$fl = $sql -> execute();	
		
		if ($fl == false)
		{
			echo "ERROR INSERT\n<br>Table: ".$tablename;
			exit;	
		}
	}	
	function addWay($tablename, $id_way, $name)
	{		
		$flag = false;
		$sql = 'INSERT INTO '.$tablename.' (id_way, name) 
				VALUES (:id_way, :name)';
		$sql = $this -> base -> prepare($sql);
		$sql -> bindParam (':id_way',$id_way);
		$sql -> bindParam (':name',$name);
		$fl = $sql -> execute();			
		if ($fl == false)
		{
			echo "ERROR INSERT\n<br>Table: ".$tablename;
			exit;
		}	
	}	
	function addNd($tablename, $id_way, $id_node)
	{		
		$flag = false;
		$sql = 'INSERT INTO '.$tablename.' (id_way, id_node) 
				VALUES (:id_way, :id_node)';
		$sql = $this -> base -> prepare($sql);
		$sql -> bindParam (':id_way',$id_way);
		$sql -> bindParam (':id_node',$id_node);
		$fl = $sql -> execute();			
		if ($fl == false)
		{
			echo "ERROR INSERT\n<br>Table: ".$tablename;
			exit;
		}	
	}
	
	function getAllNd()
	{		
		$sql = 'SELECT * FROM nd';
		$sql = $this -> base -> prepare($sql);
		$sql -> execute();
		$nds = $sql -> fetchAll();
		return $nds;
	}
	function getAllNd_distinctIdNodes()
	{
		echo "ERROR IN getAllNd_distinctIdNodes";
		exit;
	//	$sql = 'SELECT distinct(id_node) FROM nd';
		$sql = $this -> base -> prepare($sql);
		$sql -> execute();
		$nds = $sql -> fetchAll();
		return $nds;
	}
	
	function getAllNodes($start, $num)
	{
		$sql = 'SELECT * FROM nodes LIMIT '.$start.', '.$num;
		$sql = $this -> base -> prepare($sql);
		$sql -> execute();
		$nds = $sql -> fetchAll();
		return $nds;
	}
	
	function iloveyou($id)
	{
		$sql = 'UPDATE nodes SET flag = 1 WHERE id_node = :id';
		$sql = $this -> base -> prepare($sql);
		$sql -> bindParam (':id',$id);
		$fl = $sql -> execute();					
		if ($fl == false)
		{
			echo "ERROR INSERT\n<br>Table: ".$tablename;
			exit;
		}
	}
	
	function updateAllNodes($mass)
	{
		$sql = 'UPDATE nodes SET flag = 1 WHERE ';
		$n = sizeof($mass);
		foreach ($mass as $key => $one)
		{
			$sql .= "id_node = $one";
			if ($n - 1 == $key)
				$sql .= ';';
			else
				$sql .= ' or ';
		}
		$sql = $this -> base -> prepare($sql);
		$fl = $sql -> execute();					
		if ($fl == false)
		{
			echo "ERROR INSERT\n<br>Table: ".$tablename;
			exit;
		}
	}
	
	
}

function var_dump_str($data)
{
	ob_start();
	var_dump($data);
	$ret = ob_get_clean();	
	return $ret;
}


function addNode(&$base, &$nodes, $node, &$iNodes, $fl = false)
{	
	if (isset($node['lat']) and isset($node['lon']) and isset($node['id']))
	{
		$nodes[] = $node;
		$iNodes++;
	}	
	if($iNodes == 5000 or $fl and $iNodes > 0)
	{
		$base -> addAllNode('nodes', $nodes);
		$nodes = array();
		$iNodes = 0;	
	}
}

function isTag($mass, $tags)
{
	$fl = false;
	foreach($mass as $key => $one)
	{
		if (isset($tags[$key]))
		{
			foreach ($tags[$key] as $oneKey)
			{
				if ($one == $oneKey)
				{
					return true;
				}
			}
		}
	}
	return false;
}

function typeTag($mass, $tags)
{
	$fl = false;
	foreach($mass as $key => $one)
	{
		if (isset($tags[$key]))
		{
			foreach ($tags[$key] as $oneKey => $oneVal)
			{
				if ($one == $oneKey)
				{
					return $oneVal;
				}
			}
		}
	}
	return false;
}

function addPoints(&$base, &$points, $node, &$iPoints, $tags, $fl = false)
{
	if (isset($node['lat']) and isset($node['lon']) and typeTag($node['tags'], $tags) !== false)
	{	
		$node['type'] = typeTag($node['tags'], $tags);
		$points[] = $node;
		$iPoints++;
	}	
	if($iPoints == 500 or $fl and $iPoints > 0)
	{
		$base -> addMassPoints('points', $points);
		$points = array();
		$iPoints = 0;	
	}	
}

function addPoints_fromWay (&$base, &$points, $way, &$iWays, $tags, $fl = false)
{
	if (isset($way['tags']))
	{
		if (typeTag($way['tags'], $tags) !== false)
		{
			if (isset($way['nd'][0]))
			{
				$location = $base -> getPointsLocation('nodes', $way['nd'][0]);
				if (isset($location[0]))
				{
					$way['lat'] = $location[0]['lat'];
					$way['lon'] = $location[0]['lon'];
				}
			}
			if (isset($way['lat']) and isset($way['lon']))
			{	
				$way['type'] = typeTag($way['tags'], $tags);
				$points[] = $way;
				$iWays++;
			}		
		}	
	}
	if($iWays == 500 or $fl and $iWays > 0)
	{
		$base -> addMassPoints('points', $points);
		$points = array();
		$iWays = 0;	
	}
}

function addWay(&$base, &$ways, $way, &$iWays, $highwayKeys, $fl = false)
{	
	if (isset($way['id']))
	{
		if (!isset($way['tags']['name']))
		{
			$way['tags']['name'] = NULL;
		}
		if (isset($way['tags']['highway']))
		{
			if (in_array($way['tags']['highway'], $highwayKeys))
			{
				$ways[] = $way;
				$iWays++;				
			}
		}
		
	}	
	if($iWays == 1000 or $fl and $iWays > 0)
	{
		$base -> addAllWay('way', $ways);
		$base -> addAllNd('nd', $ways);
		$ways = array();
		$iWays = 0;	
	}
}
/*
CREATE TABLE `` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `price` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
*/ 