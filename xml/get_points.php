<?php
exit;
set_time_limit(0); 
ini_set("memory_limit", "1024M");

include_once "functions.php";

$base = new Base('routemaps');

$tags = array(
	'building' => array(
		'cathedral',
		'chapel',
		'church',
		),
	'leisure' => array(
		'dance',
		),
	'amenity' => array(
		'fountain',
		),
	'tourism' => array(
		'attraction',
		'artwork',
		'museum',
		'zoo',
		'viewpoint',
		),
	'historic' => array(
		'boundary_stone',
		'castle',
		),
		
);


$tags = array(
	'building' => array(
		'cathedral' => 'church',
		'chapel' => 'church',
		'church' => 'church',
		),
//	'leisure' => array(
//		'dance',
//		),
	'amenity' => array(
		'fountain' => 'monument',
		),
	'tourism' => array(
//		'attraction',
		'artwork' => 'monument',
		'museum' => 'museum',
		'zoo' => 'museum',
		'viewpoint' => 'monument',
		'theme_park' => 'park',
		),
	'historic' => array(
		'boundary_stone' => 'monument',
		'castle' => 'monument',
		'memorial' => 'monument',
		'monument' => 'monument',
		),
	'leisure' => array(
		'park' => 'park',
		),		
);

/*
ПОЛИГОНЫ
building => cathedral
building => chapel
building => church

ТОЧКИ И ПОЛИГОНЫ
leisure => dance
amenity => fountain
tourism => attraction
tourism => artwork
tourism => museum
tourism => zoo
ТОЧКИ
tourism => viewpoint
historic => boundary_stone
historic => castle
*/


$reader = new XMLReader();

$reader->open("saint-petersburg_russia.osm");

$node = '';
$points = array();
$nodeOpen = false;

$iPoints = 0;

while ($reader->read()) {
	switch ($reader->nodeType) {
		case (XMLREADER::ELEMENT):
			if ($reader->localName == "node") 
			{
				if ($nodeOpen)
				{
				//	$nodes[] = $node;
				//	var_dump($node);
				//	file_put_contents('nodes.txt', var_dump_str($node), FILE_APPEND);
				
				if ($node['name'] != NULL)
				{
					addPoints($base, $points, $node, $iPoints, $tags);
				}
					
					$node = '';
				}
				$nodeOpen = true;
				
				$node['id'] = $reader->getAttribute("id");
				$node['lat'] = $reader->getAttribute("lat");
				$node['lon'] = $reader->getAttribute("lon");

				$node['tags'] = array();
				$node['name'] = NULL;
				$node['type'] = NULL;
			}
			if ($reader->localName == "tag")
			{
				$key = $reader->getAttribute("k");
				$val = $reader->getAttribute("v");
				if ($nodeOpen)
					$node['tags'][$key] = $val;
				if ($key == 'name')
				{
					$node['name'] = $val;
				}
			}
			
			break;
			
		case (XMLREADER::END_ELEMENT):
			if ($reader->localName == "node") 
			{
				$nodeOpen = false;
			//	$nodes[] = $node;
			//	var_dump($node);
			//	file_put_contents('nodes.txt', var_dump_str($node), FILE_APPEND);
				if ($node['name'] != NULL)
				{
					addPoints($base, $points, $node, $iPoints, $tags);
				}
					
				$node = '';
			}		
	}
}


addPoints($base, $points, $node, $iPoints, $tags, true);
//addWay($base, $ways, $way, $iWays, $highwayKeys, true);
var_dump($iPoints);
echo "<hr><hr><hr><hr>";