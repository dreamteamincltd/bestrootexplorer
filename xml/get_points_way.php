<?php
exit;
set_time_limit(0); 
ini_set("memory_limit", "1024M");

include_once "functions.php";

$base = new Base('routemaps');


$tags = array(
	'building' => array(
		'cathedral',
		'chapel',
		'church',
		),
//	'leisure' => array(
//		'dance',
//		),
	'amenity' => array(
		'fountain',
		),
	'tourism' => array(
//		'attraction',
		'artwork',
		'museum',
		'zoo',
		'viewpoint',
		'theme_park',
		),
	'historic' => array(
		'boundary_stone',
		'castle',
		),
		
);


// типы в базе: музей, памятник, церковь, другие достопримечательности
//				museum, church, monument, other
$tags = array(
	'building' => array(
		'cathedral' => 'church',
		'chapel' => 'church',
		'church' => 'church',
		),
//	'leisure' => array(
//		'dance',
//		),
	'amenity' => array(
		'fountain' => 'monument',
		),
	'tourism' => array(
//		'attraction',
		'artwork' => 'monument',
		'museum' => 'museum',
		'zoo' => 'museum',
		'viewpoint' => 'monument',
		'theme_park' => 'park',
		),
	'historic' => array(
		'boundary_stone' => 'monument',
		'castle' => 'monument',
		'memorial' => 'monument',
		'monument' => 'monument',
		),
	'leisure' => array(
		'park' => 'park',
		),		
);
/*
ПОЛИГОНЫ
building => cathedral
building => chapel
building => church

ТОЧКИ И ПОЛИГОНЫ
leisure => dance
amenity => fountain
tourism => attraction
tourism => artwork
tourism => museum
tourism => zoo
ТОЧКИ
tourism => viewpoint
historic => boundary_stone
historic => castle
*/


$reader = new XMLReader();

$reader->open("saint-petersburg_russia.osm");

$way = '';
$ways = array();
$wayOpen = false;

$iPoints = 0;

while ($reader->read()) {
	switch ($reader->nodeType) {
		case (XMLREADER::ELEMENT):
			if ($reader->localName == "way") 
			{
				if ($wayOpen)
				{			
					if ($way['name'] != NULL)
					{		
						addPoints_fromWay($base, $points, $way, $iWays, $tags);
					}
					
					$way = '';
				}
				$wayOpen = true;
				
				$way['id'] = $reader->getAttribute("id");
				
				$way['tags'] = array();	
				$way['nd'] = array();	
				$way['name'] = NULL;
				$way['type'] = NULL;			
			}
			if ($reader->localName == "nd")
			{
				$val = $reader->getAttribute("ref");
				if ($wayOpen)
				{
					$way['nd'][] = $val;
				}
			}
			
			if ($reader->localName == "tag")
			{
				$key = $reader->getAttribute("k");
				$val = $reader->getAttribute("v");
				if ($wayOpen)
				{
					$way['tags'][$key] = $val;
				}
				if ($key == 'name' and $wayOpen)
				{
					$way['name'] = $val;
				}
			}
			
			break;
			
		case (XMLREADER::END_ELEMENT):
			if ($reader->localName == "way") 
			{
				$wayOpen = false;	
				if ($way['name'] != NULL)
				{
					addPoints_fromWay($base, $points, $way, $iWays, $tags);
				}
				
				$way = '';
			}	
	}
}

addPoints_fromWay($base, $points, $way, $iWays, $tags, true);
var_dump($iPoints);
echo "<hr><hr><hr><hr>";